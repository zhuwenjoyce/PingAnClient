package com.pingan.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StatusUtil {
	
	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**返回失败消息
	 * @return {"status":201,"date":"YYYY-MM-dd HH:mm:ss","data":"","message":"请求发生异常"}
	 */
	public static String fail(){
		return "{\"status\":"+StatusEnum.FAIL.getStatus()+",\"date\":\""+time()
				+"\",\"data\":\"\",\"message\":\"请求发生异常\"}";
	}
	
	/**
	 * 返回失败消息，系统繁忙
	 * @return {"status":201,"date":"YYYY-MM-dd HH:mm:ss","orderNo":"XXX","data":"","message":"系统繁忙，请稍后再试"}
	 */
	public static String systemBusy(){
		return "{\"status\":"+StatusEnum.FAIL.getStatus()+",\"date\":\""+time()
				+"\",\"data\":\"\",\"message\":\"系统繁忙，请稍后再试\"}";
	}

	
	/**
	 * 请求超时
	 * @return {"status":202,"date":"YYYY-MM-dd HH:mm:ss","data":"","message":"请求超时"}
	 */
	public static String timeout(){
		return "{\"status\":"+StatusEnum.FAIL_TIMEOUT.getStatus()+",\"date\":\""+time()
				+"\",\"data\":\"\",\"message\":\"请求超时\"}";
	}
	
	private static String time(){
		Date date = new Date();
		DATE_FORMAT.format(date);
		return DATE_FORMAT.format(date);
	}
	
	public static void main(String[] args) throws InterruptedException {
		System.out.println(time());
		Thread.sleep(1200l);
		System.out.println(time());
		Thread.sleep(1750l);
		System.out.println(time());
	}
}
