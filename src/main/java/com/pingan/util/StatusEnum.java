package com.pingan.util;

/**
 * 状态码常量类
 * @author ZhuWen
 *
 */
public enum StatusEnum {

	/**
	 * 200
	 */
	SUCCESS(200),
	/**
	 * 201
	 */
	FAIL(201), 
	/**
	 * 202
	 */
	FAIL_TIMEOUT(202);
	
	private int status;
	
	private StatusEnum(int status){
		this.status = status;
	}

	public int getStatus() {
		return status;
	}


}
