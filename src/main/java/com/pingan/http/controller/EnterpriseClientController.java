package com.pingan.http.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.pingan.http.controller.base.ClientBaseController;

/**
 * 平安行内查询：企业工商信息
 * @author ZhuWen
 *
 */
@Controller
public class EnterpriseClientController extends ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(EnterpriseClientController.class);

	@Resource(name = "configProperties")
    private Properties properties;
	
	private String findValidateCodeUrl;
	private String findResultUrl;
	
	@PostConstruct //@PostConstruct注解相当于spring配置文件中init方法
	 public void init(){
		 findValidateCodeUrl = properties.getProperty("url.enterprise.findValidatedCode").toString();
	 	 findResultUrl = properties.getProperty("url.enterprise.findResult").toString();
		 LOG.info("【企业工商信息】对象初始化完成：findValidateCodeUrl="+findValidateCodeUrl+",findResultUrl="+findResultUrl);
	 }
	
	/**
	 * @param city: 城市
	 * @param enterpriseName: 企业名称
	 * @return String
	 * 
	 */
	@RequestMapping(value="/controller/findEnterpriseValidated", method=RequestMethod.POST)
	@ResponseBody
	public String findEnterpriseValidated(@RequestParam String city, @RequestParam String enterpriseName,@RequestParam String route){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("city", city));  
        paramsList.add(new BasicNameValuePair("enterpriseName", enterpriseName));  
        paramsList.add(new BasicNameValuePair("route", route));
		LOG.info("request URL /controller/findEnterpriseValidated params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findValidateCodeUrl,paramsList);
	}
	
	/**
	 * @param city: 城市
	 * @param enterpriseName: 企业名称
	 * @param validatedCode: 验证码
	 * @param orderNo: 订单号
	 * @return String
	 * 
	 */
	@RequestMapping(value="/controller/findEnterpriseMessage", method=RequestMethod.POST)
	@ResponseBody
	public String findEnterpriseMessage(@RequestParam String city, @RequestParam String enterpriseName, @RequestParam String validatedCode, @RequestParam String orderNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("city", city));  
        paramsList.add(new BasicNameValuePair("enterpriseName", enterpriseName));  
        paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));  
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));  
		LOG.info("request URL /controller/findEnterpriseMessage params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findResultUrl,paramsList);
	}
	
	@RequestMapping("/enterprise/test")
	public String index(){
		LOG.info("request URL /enterprise/test");
		return "enterprise";
	}
}
