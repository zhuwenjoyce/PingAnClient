package com.pingan.http.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.pingan.http.controller.base.ClientBaseController;

/**
 * 平安行内查询，【组织机构代码信息】，不需要验证码
 * 
 * @author ZhuWen
 *
 */
@Controller
public class OrganizationClientController extends ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(OrganizationClientController.class);


	@Resource(name = "configProperties")
    private Properties properties;
	
	private String findResultUrl;
	
	@PostConstruct //@PostConstruct注解相当于spring配置文件中init方法
	 public void init(){
	 	 findResultUrl = properties.getProperty("url.organization.findResult").toString();
		 LOG.info("【组织机构代码信息】对象初始化完成：findResultUrl="+findResultUrl);
	 }
	
	/**
	 * @param enterpriseName: 企业名称
	 * @return String
	 * 
	 */
	@RequestMapping(value="/controller/findOrgMessage", method=RequestMethod.POST)
	@ResponseBody
	public String findOrgMessage(@RequestParam String enterpriseName){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("enterpriseName", enterpriseName));  
		LOG.info("request URL /controller/findOrgMessage params="+JSONObject.toJSONString(paramsList)); 
        return super.doPostWithParams(findResultUrl,paramsList);
	}

	@RequestMapping("/organization/test")
	public String index(){
		LOG.info("request URL /organization/test");
		return "organization";
	}
}
