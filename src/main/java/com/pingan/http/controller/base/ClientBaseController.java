package com.pingan.http.controller.base;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.pingan.util.StatusUtil;

@Controller
public class ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(ClientBaseController.class);

	private static final String CHARSET_UTF8 = "UTF-8";

	// 线程池为无限大，当执行第二个任务时如果第一个任务已经完成，会复用执行第一个任务的线程，而不用每次新建线程。
	protected static ExecutorService executorService = Executors
			.newCachedThreadPool();

	@Resource(name = "configProperties")
	private Properties properties;

	private static int requestMaxNumber;

	private static int requestTimeOutSeconds;

	private static int requestCount = 0;

	/**
	 * 行外地址IP:PORT/context
	 */
	private static String domain;

	/**
	 * 系统启动时，即调用该方法
	 */
	@PostConstruct
	public void init() {
		domain = properties.getProperty("url.domain").toString();
		LOG.info("对象初始化完成：domain=" + domain);
		requestMaxNumber = Integer.valueOf(properties.getProperty(
				"request.max.number").toString());
		requestTimeOutSeconds = Integer.valueOf(properties.getProperty(
				"request.timeout.seconds").toString());
		LOG.info("对象初始化完成：requestTimeOutSeconds=" + requestTimeOutSeconds
				+ ",requestMaxNumber=" + requestMaxNumber);
	}

	/**
	 * 执行HTTP POST请求
	 * 
	 * @param paramsList
	 */
	public String doPostWithParams(final String urlStr,
			final List<NameValuePair> paramsList) {
		requestCount++;
		// 系统繁忙时，拦截所有请求
		if (requestCount > requestMaxNumber) {
			LOG.info("因系统繁忙，请求被拦截，requestCount=" + requestCount);
			return StatusUtil.systemBusy();
		}

		String logInfo = "";
		if(urlStr.startsWith("http:")){
			logInfo = ",参数=" + JSONObject.toJSONString(paramsList)
					+ urlStr;
		}else{
			logInfo = ",参数=" + JSONObject.toJSONString(paramsList)
					+ domain + urlStr;
		}
		LOG.info("client端发起请求：" + logInfo);

		String result = StatusUtil.fail();

		// 采用线程超时机制，以免造成应用程序长时间线程堵塞
		Callable<String> callable = new Callable<String>() {
			
			@Override
			public String call() throws Exception {
				String resultStr = StatusUtil.fail();
				
				HttpClient httpClient=new DefaultHttpClient();
				HttpPost httpPost = null;
				//如果是http开头的Url地址，那么不需要拼接domain
				if(urlStr.startsWith("http:")){
					httpPost=new HttpPost(urlStr);
				}else{
					httpPost=new HttpPost(domain + urlStr);
				}
				UrlEncodedFormEntity formEntity= new UrlEncodedFormEntity(paramsList,"utf-8");
				httpPost.setEntity(formEntity);
				HttpResponse response = httpClient.execute(httpPost);
				if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
					HttpEntity responseEntity = response.getEntity();
					if (responseEntity != null) {
						resultStr = EntityUtils.toString(responseEntity, CHARSET_UTF8);
					}
				}
				
				return resultStr;
			}
		};

		try {
			// 使用一个线程执行者来启动一个设定了超时时间的线程
			Future<String> future = executorService.submit(callable);
			// 设置超时时间，若规定时间内无返回内容，中断线程执行
			result = future.get(requestTimeOutSeconds, TimeUnit.SECONDS);
		} catch (InterruptedException e) { // 主线程在等待返回结果时被中断！
			LOG.warn("主线程在等待返回结果时被中断！logInfo=" + logInfo, e);
		} catch (ExecutionException e) { // 主线程等待返回结果，但执行返回结果代码抛出异常！
			LOG.warn("主线程等待返回结果，但执行返回结果代码抛出异常！logInfo=" + logInfo, e);
		} catch (TimeoutException e) { // 主线程等待返回结果超时，因此中断任务线程！
			LOG.warn("主线程等待返回结果超时，因此中断任务线程！logInfo=" + logInfo, e);
		} catch (RejectedExecutionException e) {
			LOG.warn("executorService.submit一个线程发生Rejected异常！logInfo="
					+ logInfo, e);
		}

		requestCount--;
		LOG.info("client端结束请求：" + ",result=" + result + logInfo);
		return result;
	}
}
