package com.pingan.http.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.pingan.http.controller.base.ClientBaseController;
/**
 * 力洋车架查询
 * @author ZhuWen
 *
 */
@Controller
public class LiYangClientController extends ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(LiYangClientController.class);
	
	@Resource(name = "configProperties")
    private Properties properties;
	
	private String findResultUrl;
	
	@PostConstruct //@PostConstruct注解相当于spring配置文件中init方法
	 public void init(){
		findResultUrl = properties.getProperty("url.liyang.findResult").toString();
		 LOG.info("【力洋车架查询】对象初始化完成：findResultUrl="+findResultUrl);
	 }
	
	@RequestMapping(value="/liyang/findCXInfoByVIN", method=RequestMethod.POST)
	@ResponseBody
	public String findCXInfoByVIN(@RequestParam final String vin){
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("vin", vin));  
		LOG.info("request URL /liyang/findCXInfoByVIN vin="+vin); 
        return super.doPostWithParams(findResultUrl,paramsList);
	}
	
	@RequestMapping("/liyang/test")
	public String index(){
		LOG.info("request URL /liyang/test");
		return "liyang";
	}
}
