package com.pingan.http.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.pingan.http.controller.base.ClientBaseController;

/**
 * 平安行内查询，发票、国税
 * 
 * @author ZhuWen
 *
 */
@Controller
public class BillClientController extends ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(BillClientController.class);

//	@Resource(name = "configProperties")
//	private Properties properties;
	// 安徽
	@Value("#{configProperties['url.bill.findValidatedCode.AnHui']}")
	private String findValidateCodeAnHuiUrl;
	@Value("#{configProperties['url.bill.findResult.AnHui']}")
	private String findResultAnHuiUrl;
	// 浙江
	@Value("#{configProperties['url.bill.findValidatedCode.ZheJiang']}")
	private String findValidateCodeZheJiangUrl;
	@Value("#{configProperties['url.bill.findResult.ZheJiang']}")
	private String findResultZheJiangUrl;
	// 山东201310之后
	@Value("#{configProperties['url.bill.findValidatedCode.ShanDongAfter201310']}")
	private String findValidateCodeShanDongAfterUrl;
	@Value("#{configProperties['url.bill.findResult.ShanDongAfter201310']}")
	private String findResultShanDongAfterUrl;
	// 山东201310之前
	@Value("#{configProperties['url.bill.findValidatedCode.ShanDongBefore201310']}") 
	private String findValidateCodeShanDongBeforeUrl;
	@Value("#{configProperties['url.bill.findResult.ShanDongBefore201310']}") 
	private String findResultShanDongBeforeUrl;
	// 青岛
	@Value("#{configProperties['url.bill.findValidatedCode.Qingdao']}")
	private String findValidateCodeQingdaoUrl;
	@Value("#{configProperties['url.bill.findResult.Qingdao']}")
	private String findResultQingdaoUrl;
	// 北京
	@Value("#{configProperties['url.bill.findValidatedCode.BeiJing']}")
	private String findValidateCodeBeiJingUrl;
	@Value("#{configProperties['url.bill.findResult.BeiJing']}")
	private String findResultBeiJingUrl;
	// 不区分省市的查询发票接口
	@Value("#{configProperties['url.bill.findValidatedCode']}")  
	private String findValidateCodeUrl;
	@Value("#{configProperties['url.bill.findResult']}")  
	private String findResultUrl;
	
	@PostConstruct
	// @PostConstruct注解相当于spring配置文件中init方法
	public void init() {		
		String infoLog = "【发票、国税】对象初始化完成：findValidateCodeAnHuiUrl=" + findValidateCodeAnHuiUrl + ",findResultAnHuiUrl=" + findResultAnHuiUrl
				+",findValidateCodeZheJiangUrl=" + findValidateCodeZheJiangUrl + ",findResultZheJiangUrl=" + findResultZheJiangUrl
				+",findValidateCodeShanDongAfterUrl=" + findValidateCodeShanDongAfterUrl + ",findResultShanDongAfterUrl=" + findResultShanDongAfterUrl
				+",findValidateCodeShanDongBeforeUrl=" + findValidateCodeShanDongBeforeUrl + ",findResultShanDongBeforeUrl=" + findResultShanDongBeforeUrl
				+",findValidateCodeQingdaoUrl=" + findValidateCodeQingdaoUrl + ",findResultQingdaoUrl=" + findResultQingdaoUrl
				+",findValidateCodeBeiJingUrl=" + findValidateCodeBeiJingUrl + ",findResultBeiJingUrl=" + findResultBeiJingUrl
				+",findValidateCodeUrl=" + findValidateCodeUrl + ",findResultUrl=" + findResultUrl;
		LOG.info(infoLog);
	}

	/*
	 * 安徽
	 */
	@RequestMapping(value = "/bill/findAnHuiValidatedCode", method = RequestMethod.POST)
	@ResponseBody
	public String findAnHuiValidatedCode(@RequestParam final String IRSName,
			@RequestParam final String city,
			@RequestParam final String billCode,
			@RequestParam final String billNo, @RequestParam final String amount) {
		// 创建参数集合
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("IRSName", IRSName));
		paramsList.add(new BasicNameValuePair("city", city));
		paramsList.add(new BasicNameValuePair("billCode", billCode));
		paramsList.add(new BasicNameValuePair("billNo", billNo));
		paramsList.add(new BasicNameValuePair("amount", amount));
		LOG.info("request URL /bill/findAnHuiValidatedCode params="+JSONObject.toJSONString(paramsList));
		return super.doPostWithParams(findValidateCodeAnHuiUrl, paramsList);
	}

	@RequestMapping(value = "/bill/findAnHuiBillInfo", method = RequestMethod.POST)
	@ResponseBody
	public String findAnHuiBillInfo(@RequestParam final String validatedCode,
			@RequestParam final String orderNo) {
		// 创建参数集合
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));
		paramsList.add(new BasicNameValuePair("orderNo", orderNo));
		LOG.info("request URL /bill/findAnHuiBillInfo params="+JSONObject.toJSONString(paramsList));
		return super.doPostWithParams(findResultAnHuiUrl, paramsList);
	}

	/*
	 * 浙江
	 */

	@RequestMapping(value = "/bill/findZheJiangValidatedCode", method = RequestMethod.POST)
	@ResponseBody
	public String findZheJiangValidatedCode(
			@RequestParam final String IRSName,
			@RequestParam final String billCode,
			@RequestParam final String billNo,
			@RequestParam final String taxpayerId,
			@RequestParam final String billingTime,
			@RequestParam final String billingAmount) {
		// 创建参数集合
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("IRSName", IRSName));
		paramsList.add(new BasicNameValuePair("billCode", billCode));
		paramsList.add(new BasicNameValuePair("billNo", billNo));
		paramsList.add(new BasicNameValuePair("taxpayerId", taxpayerId));
		paramsList.add(new BasicNameValuePair("billingTime", billingTime));
		paramsList.add(new BasicNameValuePair("billingAmount", billingAmount));
		LOG.info("request URL /bill/findZheJiangValidatedCode params="+JSONObject.toJSONString(paramsList));
		return super.doPostWithParams(findValidateCodeZheJiangUrl, paramsList);
	}

	@RequestMapping(value = "/bill/findZheJiangBillInfo", method = RequestMethod.POST)
	@ResponseBody
	public String findZheJiangBillInfo(
			@RequestParam final String validatedCode,
			@RequestParam final String orderNo) {
		// 创建参数集合
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));
		paramsList.add(new BasicNameValuePair("orderNo", orderNo));
		LOG.info("request URL /bill/findZheJiangBillInfo params="+JSONObject.toJSONString(paramsList));
		return super.doPostWithParams(findResultZheJiangUrl, paramsList);
	}
	
	/**
	 * 
	 *山东（除青岛外），201310之后
	 * 
	 */
	@RequestMapping(value="/bill/findShanDongAfter201310ValidatedCode", method=RequestMethod.POST)
	@ResponseBody
	public String findShanDongAfter201310ValidatedCode(@RequestParam final String IRSName, 
			@RequestParam final String billCode, @RequestParam final String billNo, 
			@RequestParam final String taxpayerId,@RequestParam final String taxpayerName){
		// 创建参数集合 
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("IRSName", IRSName));
		paramsList.add(new BasicNameValuePair("billCode", billCode));
		paramsList.add(new BasicNameValuePair("billNo", billNo));
		paramsList.add(new BasicNameValuePair("taxpayerId", taxpayerId));
		paramsList.add(new BasicNameValuePair("taxpayerName", taxpayerName));
		LOG.info("request URL /bill/findShanDongAfter201310ValidatedCode params="+JSONObject.toJSONString(paramsList));
		return super.doPostWithParams(findValidateCodeShanDongAfterUrl, paramsList);
	}
	
	@RequestMapping(value="/bill/findShanDongAfter201310BillInfo", method=RequestMethod.POST)
	@ResponseBody
	public String findShanDongAfter201310BillInfo(@RequestParam final String validatedCode, @RequestParam final String orderNo){
		// 创建参数集合
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));
		paramsList.add(new BasicNameValuePair("orderNo", orderNo));
		LOG.info("request URL /bill/findShanDongAfter201310BillInfo params="+JSONObject.toJSONString(paramsList));
		return super.doPostWithParams(findResultShanDongAfterUrl, paramsList);
	}

	
	/**
	 * 
	 *山东（除青岛外），201310之前
	 * 
	 */
	@RequestMapping(value="/bill/findShanDongBefore201310ValidatedCode", method=RequestMethod.POST)
	@ResponseBody
	public String findShanDongBefore201310ValidatedCode(@RequestParam final String IRSName, 
			@RequestParam final String billCode, @RequestParam final String billNo){
		// 创建参数集合 
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("IRSName", IRSName));
		paramsList.add(new BasicNameValuePair("billCode", billCode));
		paramsList.add(new BasicNameValuePair("billNo", billNo));
		LOG.info("request URL /bill/findShanDongBefore201310ValidatedCode params="+JSONObject.toJSONString(paramsList));
		return super.doPostWithParams(findValidateCodeShanDongBeforeUrl, paramsList);
	}
	
	@RequestMapping(value="/bill/findShanDongBefore201310BillInfo", method=RequestMethod.POST)
	@ResponseBody
	public String findShanDongBefore201310BillInfo(@RequestParam final String validatedCode, @RequestParam final String orderNo){
		// 创建参数集合
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));
		paramsList.add(new BasicNameValuePair("orderNo", orderNo));
		LOG.info("request URL /bill/findShanDongBefore201310BillInfo params="+JSONObject.toJSONString(paramsList));
		return super.doPostWithParams(findResultShanDongBeforeUrl, paramsList);
	}
	
	/*
	 * 青岛
	 * 
	 */
	@RequestMapping(value="/bill/findQingdaoValidatedCode", method=RequestMethod.POST)
	@ResponseBody
	public String findQingdaoValidatedCode(@RequestParam final String IRSName,
			@RequestParam final String billCode, @RequestParam final String billNo, 
			@RequestParam final String taxpayerId,@RequestParam final String id,
			@RequestParam final String billingAmount,@RequestParam final String addedTax,@RequestParam final String billingTime,
			@RequestParam final String haveTaxCode,@RequestParam final String isNetworkInvoice){
		// 创建参数集合 
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("IRSName", IRSName));
		paramsList.add(new BasicNameValuePair("billCode", billCode));
		paramsList.add(new BasicNameValuePair("billNo", billNo));
		paramsList.add(new BasicNameValuePair("taxpayerId", taxpayerId));
		paramsList.add(new BasicNameValuePair("id", id));
		paramsList.add(new BasicNameValuePair("billingAmount", billingAmount));
		paramsList.add(new BasicNameValuePair("addedTax", addedTax));
		paramsList.add(new BasicNameValuePair("billingTime", billingTime));
		paramsList.add(new BasicNameValuePair("haveTaxCode", haveTaxCode));
		paramsList.add(new BasicNameValuePair("isNetworkInvoice", isNetworkInvoice));
		return super.doPostWithParams(findValidateCodeQingdaoUrl, paramsList);
	}
	
	@RequestMapping(value="/bill/findQingdaoBillInfo", method=RequestMethod.POST)
	@ResponseBody
	public String findQingdaoBillInfo(@RequestParam final String validatedCode, @RequestParam final String orderNo){
		// 创建参数集合
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));
		paramsList.add(new BasicNameValuePair("orderNo", orderNo));
		return super.doPostWithParams(findResultQingdaoUrl, paramsList);
	}
	
	/*
	 * 北京
	 * 
	 */
	@RequestMapping(value="/bill/findBeiJingValidatedCode", method=RequestMethod.POST)
	@ResponseBody
	public String findBeiJingValidatedCode(@RequestParam final String IRSName, 
			@RequestParam final String billCode, @RequestParam final String billNo, 
			@RequestParam final String billingTime,@RequestParam final String billingAmount, @RequestParam final String taxpayerId){
		// 创建参数集合 "IRSName","billCode","billNo","billingTime","billingAmount","taxpayerId"
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("IRSName", IRSName));
		paramsList.add(new BasicNameValuePair("billCode", billCode));
		paramsList.add(new BasicNameValuePair("billNo", billNo));
		paramsList.add(new BasicNameValuePair("billingTime", billingTime));
		paramsList.add(new BasicNameValuePair("billingAmount", billingAmount));
		paramsList.add(new BasicNameValuePair("taxpayerId", taxpayerId));
		return super.doPostWithParams(findValidateCodeBeiJingUrl, paramsList);
	}	
	
	@RequestMapping(value="/bill/findBeiJingBillInfo", method=RequestMethod.POST)
	@ResponseBody
	public String findBeiJingBillInfo(@RequestParam final String validatedCode, @RequestParam final String orderNo){
		// 创建参数集合
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));
		paramsList.add(new BasicNameValuePair("orderNo", orderNo));
		return super.doPostWithParams(findResultBeiJingUrl, paramsList);
	}
	
	/**
	 * 不区分省市的查询发票接口,第一步获取验证码
	 * @param billCode  发票代码
	 * @param billNo    发票号码
	 * @param occurDate 开票日期
	 * @param amount    开票金额（不含税）
	 * @return
	 */
	@RequestMapping(value="/bill/findValidatedCode", method=RequestMethod.POST)
	@ResponseBody
	public String findValidatedCode(@RequestParam final String billCode,@RequestParam final String billNo,@RequestParam final String occurDate,@RequestParam final String amount){
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("billCode", billCode));
		paramsList.add(new BasicNameValuePair("billNo", billNo));
		paramsList.add(new BasicNameValuePair("occurDate", occurDate));
		paramsList.add(new BasicNameValuePair("amount", amount));
		return super.doPostWithParams(findValidateCodeUrl, paramsList);
	}
	/**
	 * 不区分省市的查询发票接口,第二步获取发票结果
	 * @param orderNo        订单号
	 * @param validatedCode  验证码
	 * @return
	 */
	@RequestMapping(value="/bill/findResult", method=RequestMethod.POST)
	@ResponseBody
	public String findResult(@RequestParam final String orderNo, @RequestParam final String validatedCode){
		// 创建参数集合
		List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
		paramsList.add(new BasicNameValuePair("orderNo", orderNo));
		paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));
		return super.doPostWithParams(findResultUrl, paramsList);
	}

	@RequestMapping("/bill/test")
	public String index(){
		LOG.info("request URL /bill/test");
		return "bill";
	}
}
