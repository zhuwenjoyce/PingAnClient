package com.pingan.http.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.pingan.http.controller.base.ClientBaseController;

/**
 * 平安行内查询，【电话号码】，不需要验证码
 * 
 * @author ZhuWen
 *
 */
@Controller
public class PhoneClientController extends ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(PhoneClientController.class);


	@Resource(name = "configProperties")
    private Properties properties;
	
	private String findResultNumberUrl;
	
	@PostConstruct //@PostConstruct注解相当于spring配置文件中init方法
	 public void init(){
	 	 findResultNumberUrl = properties.getProperty("url.phone.findResult.number").toString();
		 LOG.info("【电话号码】对象初始化完成：findResultNumberUrl="+findResultNumberUrl);
	 }
	
	/**
	 * @param telNum 电话号码，举例：13333333333
	 * @return 
			 { "status":200,"date":"*","orderNo":"*",
			  "data":{
			         [{"title":"*","url":"*"},{"title":"*","url":"*"},{"title":"*","url":"*"}]
			 }, 
			 "message":"成功！"}
	 */
	@RequestMapping(value="/controller/phone/findResult/number", method=RequestMethod.POST)
	@ResponseBody
	public String findPhoneNumber(@RequestParam String telNum){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("telNum", telNum));  
		LOG.info("request URL /controller/phone/number params="+JSONObject.toJSONString(paramsList)); 
        return super.doPostWithParams(findResultNumberUrl,paramsList);
	}

	@RequestMapping("/phone/test")
	public String index(){
		LOG.info("request URL /phone/test");
		return "phone";
	}
}
