package com.pingan.http.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.pingan.http.controller.base.ClientBaseController;

/**
 * 行内获取【违章、计分、报废、车价】
 * 
 * @author ZhuWen
 *
 */
@Controller
public class VehicleClientController extends ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(VehicleClientController.class);

	@Resource(name = "configProperties")
    private Properties properties;

	// 违章
	private String findValidateCodePeccancyUrl;
	private String findResultPeccancyUrl;
	// 计分
	private String findValidateCodeScoringUrl;
	private String findResultScoringUrl;
	// 报废
	private String findValidateCodeScrapUrl;
	private String findResultScrapUrl;
	// 车价
	private String findResultVehiclePriceUrl;
	// 汽车图片
	private String findResultVehiclePictureUrl;
	
	@PostConstruct //@PostConstruct注解相当于spring配置文件中init方法
	public void init(){
		findValidateCodePeccancyUrl = properties.getProperty("url.vehicle.findValidatedCode.peccancy").toString();
		findResultPeccancyUrl = properties.getProperty("url.vehicle.findResult.peccancy").toString();
		findValidateCodeScoringUrl = properties.getProperty("url.vehicle.findValidatedCode.scoring").toString();
		findResultScoringUrl = properties.getProperty("url.vehicle.findResult.scoring").toString();
		findValidateCodeScrapUrl = properties.getProperty("url.vehicle.findValidatedCode.scrap").toString();
		findResultScrapUrl = properties.getProperty("url.vehicle.findResult.scrap").toString();
		findResultVehiclePriceUrl = properties.getProperty("url.vehicle.findResult.price").toString();
		findResultVehiclePictureUrl = properties.getProperty("url.vehicle.findResult.picture").toString();
		
		String infoLog = "【违章】对象初始化完成：findValidateCodePeccancyUrl="+findValidateCodePeccancyUrl+",findResultPeccancyUrl="+findResultPeccancyUrl
				+"【计分】对象初始化完成：findValidateCodeScoringUrl="+findValidateCodeScoringUrl+",findResultScoringUrl="+findResultScoringUrl
				+"【报废】对象初始化完成：findValidateCodeScrapUrl="+findValidateCodeScrapUrl+",findResultScrapUrl="+findResultScrapUrl
				+"【车价】对象初始化完成：findResultVehiclePriceUrl="+findResultVehiclePriceUrl
				+"【汽车图片】对象初始化完成：findResultVehiclePictureUrl="+findResultVehiclePictureUrl;
		LOG.info(infoLog);
				
	 }
	
	/**
	 * 查询【违章】第一步，获取验证码
	 * @param province    省份名称
	 * @param city        市
	 * @param plateType   号牌种类
	 * @param plateCode   号牌号码
	 * @param engineCode  发动机后六位
	 * @return
	 */
	@RequestMapping(value="/controller/vehicle/findValidated/peccancy", method=RequestMethod.POST)
	@ResponseBody
	public String findValidatedPeccancy(@RequestParam String province, @RequestParam String city
			, @RequestParam String plateType, @RequestParam String plateCode, @RequestParam String engineCode){
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("province", province));
        paramsList.add(new BasicNameValuePair("city", city));  
        paramsList.add(new BasicNameValuePair("plateType", plateType));  
        paramsList.add(new BasicNameValuePair("plateCode", plateCode));  
        paramsList.add(new BasicNameValuePair("engineCode", engineCode));  
		LOG.info("request URL /controller/vehicle/findValidated/peccancy params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findValidateCodePeccancyUrl,paramsList);
	}
	
	/**
	 * 查询【违章】第二步，输入验证码，获取结果
	 * @param validatedCode
	 * @param orderNo
	 * @return
	 */
	@RequestMapping(value="/controller/vehicle/findResult/peccancy", method=RequestMethod.POST)
	@ResponseBody
	public String findResultPeccancy(@RequestParam String validatedCode, @RequestParam String orderNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));  
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));  
		LOG.info("request URL /controller/vehicle/findResult/peccancy params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findResultPeccancyUrl,paramsList);
	}
	
	/**
	 * 查询【计分】第一步，获取验证码
	 * @param province      省份名称
	 * @param city          市
	 * @param identityCode  证号
	 * @param fileCode      档案编号
	 * @return
	 */
	@RequestMapping(value="/controller/vehicle/findValidated/scoring", method=RequestMethod.POST)
	@ResponseBody
	public String findValidatedScoring(@RequestParam String province, @RequestParam String city
			, @RequestParam String identityCode, @RequestParam String fileCode){
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("province", province));
        paramsList.add(new BasicNameValuePair("city", city));  
        paramsList.add(new BasicNameValuePair("identityCode", identityCode));  
        paramsList.add(new BasicNameValuePair("fileCode", fileCode));  
		LOG.info("request URL /controller/vehicle/findValidated/scoring params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findValidateCodeScoringUrl,paramsList);
	}
	
	/**
	 * 查询【计分】第二步，输入验证码，获取结果
	 * @param validatedCode
	 * @param orderNo
	 * @return
	 */
	@RequestMapping(value="/controller/vehicle/findResult/scoring", method=RequestMethod.POST)
	@ResponseBody
	public String findResultScoring(@RequestParam String validatedCode, @RequestParam String orderNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));  
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));  
		LOG.info("request URL /controller/vehicle/findResult/scoring params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findResultScoringUrl,paramsList);
	}
	
	/**
	 * 查询【报废】第一步，获取验证码
	 * @param province  省份名称
	 * @param city      市
	 * @param plateType 号牌种类
	 * @param plateCode 号牌号码
	 * @return
	 */
	@RequestMapping(value="/controller/vehicle/findValidated/scrap", method=RequestMethod.POST)
	@ResponseBody
	public String findValidatedScrap(@RequestParam String province, @RequestParam String city
			, @RequestParam String plateType, @RequestParam String plateCode){
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("province", province));
        paramsList.add(new BasicNameValuePair("city", city));  
        paramsList.add(new BasicNameValuePair("plateType", plateType));  
        paramsList.add(new BasicNameValuePair("plateCode", plateCode));  
		LOG.info("request URL /controller/vehicle/findValidated/scrap params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findValidateCodeScrapUrl,paramsList);
	}
	
	/**
	 * 查询【报废】第二步，输入验证码，获取结果
	 * @param validatedCode
	 * @param orderNo
	 * @return
	 */
	@RequestMapping(value="/controller/vehicle/findResult/scrap", method=RequestMethod.POST)
	@ResponseBody
	public String findResultScrap(@RequestParam String validatedCode, @RequestParam String orderNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));  
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));  
		LOG.info("request URL /controller/vehicle/findResult/scrap params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findResultScrapUrl,paramsList);
	}
	
	/**
	 * 查询【车价】，仅一步，直接返回查询结果，无需获取验证码
	 * @param wwwName    评估网站名字
	 * @param carInfo    车型
	 * @param year       上牌年份
	 * @param mouth      上牌月份
	 * @param plateCode  公里数
	 * @param engineCode 地区
	 * @return
	 */
	@RequestMapping(value="/controller/vehicle/findResult/price", method=RequestMethod.POST)
	@ResponseBody
	public String findResultVechilePrice(@RequestParam String wwwName, @RequestParam String carInfo,@RequestParam String year
			, @RequestParam String mouth,@RequestParam String plateCode,@RequestParam String engineCode){
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("wwwName", wwwName));
        paramsList.add(new BasicNameValuePair("carInfo", carInfo));
        paramsList.add(new BasicNameValuePair("year", year));
        paramsList.add(new BasicNameValuePair("mouth", mouth));
        paramsList.add(new BasicNameValuePair("plateCode", plateCode));  
        paramsList.add(new BasicNameValuePair("engineCode", engineCode)); 
		LOG.info("request URL /controller/vehicle/findResult/price params="+JSONObject.toJSONString(paramsList)); 
        return super.doPostWithParams(findResultVehiclePriceUrl,paramsList);
	}
	
	/**
	 * 查询【汽车图片】，仅一步，直接返回查询结果，无需获取验证码
	 * @param carBrand    品牌
	 * @param carType     类型
	 * @param carSer      车系
	 * @param carOutYear  年份
	 * @param carModel    车型
	 * @return
	 */
	@RequestMapping(value="/controller/vehicle/findResult/picture", method=RequestMethod.POST)
	@ResponseBody
	public String findResultVechilePicture(@RequestParam String carBrand, @RequestParam String carType, @RequestParam String carSer
			, @RequestParam String carOutYear, @RequestParam String carModel){
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("carBrand", carBrand));
        paramsList.add(new BasicNameValuePair("carType", carType));
        paramsList.add(new BasicNameValuePair("carSer", carSer));
        paramsList.add(new BasicNameValuePair("carOutYear", carOutYear));
        paramsList.add(new BasicNameValuePair("carModel", carModel));
		LOG.info("request URL /controller/vehicle/findResult/picture params="+JSONObject.toJSONString(paramsList)); 
        return super.doPostWithParams(findResultVehiclePictureUrl,paramsList);
	}
	
	@RequestMapping("/vehicle/test")
	public String index(){
		LOG.info("request URL /vehicle/test");
		return "vehicle";
	}
}
