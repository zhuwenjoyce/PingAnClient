package com.pingan.http.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.pingan.http.controller.base.ClientBaseController;

/**
 * 平安行内查询：保单<br>
 * 太平洋保险、平安财险、平安人寿
 * 
 * @author ZhuWen
 *
 */
@Controller
public class PolicyClientController extends ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(PolicyClientController.class);
	
	@Resource(name = "configProperties")
    private Properties properties;
	/**
	 * 太平洋保险
	 */
	private String findValidateCodeForCpicUrl;
	private String findResultForCpicUrl;
	/**
	 * 平安财险
	 */
	private String findValidateCodeForPropertyUrl;
	private String findResultForPropertyUrl;
	/**
	 * 平安人寿
	 */
	private String findValidateForLifeInsuranceUrl;
	private String findResultForLifeInsuranceUrl;
		
	@PostConstruct //@PostConstruct注解相当于spring配置文件中init方法
	 public void init(){
		 findValidateCodeForCpicUrl = properties.getProperty("url.policy.findValidatedCode.cpic").toString();
		 findResultForCpicUrl = properties.getProperty("url.policy.findResult.cpic").toString();
		 findValidateCodeForPropertyUrl = properties.getProperty("url.policy.findValidatedCode.PingAnPropertyCasualty").toString();
		 findResultForPropertyUrl = properties.getProperty("url.policy.findResult.PingAnPropertyCasualty").toString();
		 findValidateForLifeInsuranceUrl = properties.getProperty("url.policy.findValidatedCode.LifeInsurance").toString();
		 findResultForLifeInsuranceUrl = properties.getProperty("url.policy.findResult.LifeInsurance").toString();
		 String infoLog = "【保单】对象初始化完成：findValidateCodeForCpicUrl="+findValidateCodeForCpicUrl
				 +",findResultForCpicUrl="+findResultForCpicUrl
				 +",findValidateCodeForPropertyUrl="+findValidateCodeForPropertyUrl
				 +",findResultForPropertyUrl="+findResultForPropertyUrl
				 +",findValidateForLifeInsuranceUrl="+findValidateForLifeInsuranceUrl
				 +",findResultForLifeInsuranceUrl="+findResultForLifeInsuranceUrl;
		 LOG.info(infoLog);
	 }
	
	/**
	 * 太平洋保险第一步，获取 验证码
	 * @param companyName: 保险公司名称
	 * @return String
	 * 
	 */
	@RequestMapping(value="/policy/findCPICValidatedCode", method=RequestMethod.POST)
	@ResponseBody
	public String findCPICValidated(@RequestParam final String companyName, @RequestParam final String certificateType, 
			@RequestParam final String id, @RequestParam final String policyNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("companyName", companyName));  
        paramsList.add(new BasicNameValuePair("certificateType", certificateType));  
        paramsList.add(new BasicNameValuePair("id", id));  
        paramsList.add(new BasicNameValuePair("policyNo", policyNo));  
		LOG.info("request URL /policy/findCPICValidatedCode params="+JSONObject.toJSONString(paramsList)); 
        return super.doPostWithParams(findValidateCodeForCpicUrl,paramsList);
	}
	
	/**
	 * 太平洋保险第二步，输入验证码，获取结果
	 * @param validatedCode: 验证码
	 * @param orderNo: 订单号
	 * @return String
	 * 
	 */
	@RequestMapping(value="/policy/findCPICMessage", method=RequestMethod.POST)
	@ResponseBody
	public String findCPICMessage(@RequestParam final String validatedCode, @RequestParam final String orderNo){
		// 创建参数集合 
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));  
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));   
		LOG.info("request URL /policy/findCPICMessage params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findResultForCpicUrl,paramsList);
	}
	
	/**
	 * 平安财险第一步，获取验证码
	 * @param companyName
	 * @param policyType
	 * @param policyNo
	 * @param PolicyVerificationCode
	 * @return
	 */
	@RequestMapping(value="/policy/findPingAnPropertyCasualtyValidatedCode", method=RequestMethod.POST)
	@ResponseBody
	public String findPingAnPropertyCasualtyValidated(@RequestParam final String companyName,
			@RequestParam final String policyType, @RequestParam final String policyNo, 
			@RequestParam final String PolicyVerificationCode){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("companyName", companyName));  
        paramsList.add(new BasicNameValuePair("policyType", policyType));    
        paramsList.add(new BasicNameValuePair("policyNo", policyNo));    
        paramsList.add(new BasicNameValuePair("PolicyVerificationCode", PolicyVerificationCode));   
		LOG.info("request URL /policy/findPingAnPropertyCasualtyValidatedCode params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findValidateCodeForPropertyUrl,paramsList);
	}
	
	/**
	 * 平安财险第二步，输入验证码，获取结果
	 * @param validatedCode: 验证码
	 * @param orderNo: 订单号
	 * @return String
	 * 
	 */
	@RequestMapping(value="/policy/findPingAnPropertyCasualtyMessage", method=RequestMethod.POST)
	@ResponseBody
	public String findPingAnPropertyCasualtyMessage(@RequestParam final String validatedCode, @RequestParam final String orderNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));  
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));    
		LOG.info("request URL /policy/findPingAnPropertyCasualtyMessage params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findResultForPropertyUrl,paramsList);
	}
	
	/**平安人寿第一步，获取验证码
	 * 
	 * @param companyName 保险公司名称
	 * @param userName  投保人名称
	 * @param birthday  投保人生日 
	 * @param policyNo  保单号  
	 * @return
	 */
	@RequestMapping(value="/policy/findValidatedCode/lifeInsurance", method=RequestMethod.POST)
	@ResponseBody
	public String findPingAnLifeInsuranceValidated(@RequestParam final String companyName, @RequestParam final String userName,
			@RequestParam final String birthday, @RequestParam final String policyNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("companyName", companyName));  
        paramsList.add(new BasicNameValuePair("userName", userName));  
        paramsList.add(new BasicNameValuePair("birthday", birthday));   
        paramsList.add(new BasicNameValuePair("policyNo", policyNo));   
		LOG.info("request URL /policy/findValidatedCode/lifeInsurance params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findValidateForLifeInsuranceUrl,paramsList);
	}
	
	/**
	 * 平安人寿第二步，输入验证码，获取结果
	 * @param validatedCode: 验证码
	 * @param orderNo: 订单号
	 * @return String
	 * 
	 */
	@RequestMapping(value="/policy/findResult/lifeInsurance", method=RequestMethod.POST)
	@ResponseBody
	public String findPingAnLifeInsuranceMessage(@RequestParam final String validatedCode, @RequestParam final String orderNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));  
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));    
		LOG.info("request URL /policy/findResult/lifeInsurance params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findResultForLifeInsuranceUrl,paramsList);
	}
	
	/**
	 * 跳转到【保单】的测试页面
	 * @return
	 */
	@RequestMapping("/policy/test")
	public String index(){
		LOG.info("request URL /policy/test");
		return "policy";
	}
}
