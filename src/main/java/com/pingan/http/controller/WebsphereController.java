package com.pingan.http.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.pingan.http.controller.base.ClientBaseController;
/**
 * websphere服务器的测试类，与实际业务查询无关。<br>
 * 用于测试在断网环境下，websphere服务器能否正常启动项目，以及正常发出http请求。
 * @author ZhuWen
 */
@Controller
public class WebsphereController extends ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(WebsphereController.class);
	
	/**
	 * 发起请求到本类，测试websphere的http通讯情况
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="/ws/start/post")
	@ResponseBody
	public String startHttpPost(HttpServletRequest request,HttpServletResponse response, @RequestParam String name,@RequestParam String city){
		String contextPath = request.getServletContext().getContextPath();
		int localPort = request.getLocalPort();
		
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("name", name));  
        paramsList.add(new BasicNameValuePair("city", city));  
		LOG.info("request URL /ws/post 测试websphere服务器中，发起http请求，params="+JSONObject.toJSONString(paramsList));  
        return super.doPostWithParams("http://localhost:"+localPort+contextPath+"/ws/receive/post.do",paramsList);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/ws/receive/post")
	@ResponseBody
	public Map receiveHttpPost(@RequestParam String name,@RequestParam String city){
		LOG.info("request URL /ws/receive/post 测试websphere服务器中，接收http请求，params="+name+",city="+city);  
		Map map = new HashMap();
		map.put("name", name);
		map.put("city", city);
		map.put("age", new Random().nextInt(50)+10);
		return map;
	}
	
	@RequestMapping("/ws/test")
	public String index(){
		LOG.info("request URL /ws/test");
		return "websphere";
	}
	

}
