package com.pingan.http.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.pingan.http.controller.base.ClientBaseController;

/**
 * 行内获取【法院执行信息（个人、企业）】
 * 
 * @author ZhuWen
 *
 */
@Controller
public class CourtClientController extends ClientBaseController {
	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(CourtClientController.class);

	@Resource(name = "configProperties")
    private Properties properties;
	
	private String findValidateCodeUrl;
	private String findValidateCode2Url;
	private String findResultUrl;
	
	@PostConstruct //@PostConstruct注解相当于spring配置文件中init方法
	 public void init(){
		 findValidateCodeUrl = properties.getProperty("url.court.findValidatedCode").toString();
		 findValidateCode2Url = properties.getProperty("url.court.findValidated2Code").toString();
	 	 findResultUrl = properties.getProperty("url.court.findResult").toString();
		 LOG.info("【法院执行信息（个人、企业）】对象初始化完成：findValidateCodeUrl="+findValidateCodeUrl+",findResultUrl="+findResultUrl+",findValidateCode2Url="+findValidateCode2Url);
	 }
	
	/**
	 * 第一次获取验证码
	 * @param name: 被执行人姓名/名称
	 * @param code: 身份证号码/组织机构代码
	 * @return String
	 * 
	 */
	@RequestMapping(value="/controller/findExecutorValidated", method=RequestMethod.POST)
	@ResponseBody
	public String findExecutorValidated(@RequestParam String name, @RequestParam String code,@RequestParam String route){
        // 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("name", name));  
        paramsList.add(new BasicNameValuePair("code", code));
		paramsList.add(new BasicNameValuePair("route",route));
		LOG.info("request URL /controller/findExecutorValidated params="+JSONObject.toJSONString(paramsList));
        return super.doPostWithParams(findValidateCodeUrl,paramsList);
	}
	/**
	 * 第二次获取验证码
	 * @param name
	 * @param code
	 * @return
	 */
	@RequestMapping(value="/controller/findExecutorValidated2", method=RequestMethod.POST)
	@ResponseBody
	public String findExecutorValidated2(@RequestParam String name, @RequestParam String code,@RequestParam String validatedCode,@RequestParam String orderNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("name", name));  
        paramsList.add(new BasicNameValuePair("code", code)); 
        paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));   
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));  
		LOG.info("request URL /controller/findExecutorValidated2 params="+JSONObject.toJSONString(paramsList)); 
        return super.doPostWithParams(findValidateCode2Url,paramsList);
	}
	
	/**
	 * 最后一步拿结果
	 * @param name
	 * @param code
	 * @param validatedCode
	 * @param orderNo
	 * @return
	 */
	@RequestMapping(value="/controller/findExecutorMessage", method=RequestMethod.POST)
	@ResponseBody
	public String findExecutorMessage(@RequestParam String name, @RequestParam String code, @RequestParam String validatedCode, @RequestParam String orderNo){
		// 创建参数集合
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();  
        paramsList.add(new BasicNameValuePair("name", name));  
        paramsList.add(new BasicNameValuePair("code", code));  
        paramsList.add(new BasicNameValuePair("validatedCode", validatedCode));  
        paramsList.add(new BasicNameValuePair("orderNo", orderNo));
		LOG.info("request URL /controller/findExecutorMessage params="+JSONObject.toJSONString(paramsList));  
        return super.doPostWithParams(findResultUrl,paramsList);
	}
	
	@RequestMapping("/court/test")
	public String index(){
		LOG.info("request URL /court/test");
		return "court";
	}
}
