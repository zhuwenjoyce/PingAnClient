<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- java class: BillClientController.java url: /bill/test-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>测试|发票国税</title>
<style type="text/css">
input[type='text'] {
	width: 350px;
	height: 15px;
}
</style>
<script src="<%=request.getContextPath()%>/res/jquery-3.1.1.js"></script>
<script src="<%=request.getContextPath()%>/res/jquery.form.values.js"></script>
<script src="<%=request.getContextPath()%>/res/date.format.js"></script>
</head>
<body>
	<h3>
		<a href="<%=request.getContextPath()%>">返回首页</a>
	</h3>
	<p>【安徽】第一步，获取验证码</p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findAnHuiValidatedCode.do">
			<table>
				<tr>
					<td>国税局名称:IRSName</td>
					<td><input type="text" name="IRSName"/></td>
				</tr>
				<tr>
					<td>发票开具单位所属地市:city</td>
					<td><input type="text" name="city"/></td>
				</tr>
				<tr>
					<td>发票代码:billCode</td>
					<td><input type="text" name="billCode"/></td>
				</tr>
				<tr>
					<td>发票号码:billNo</td>
					<td><input type="text" name="billNo"/></td>
				</tr>
				<tr>
					<td>金额合计:amount</td>
					<td><input type="text" name="amount"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>

	<p>【安徽】第二步，获取结果</p>
	<form method="post" action="<%=request.getContextPath()%>/bill/findAnHuiBillInfo.do">
			<table>
				<tr>
					<td>验证码:validatedCode</td>
					<td><input type="text" name="validatedCode"/></td>
				</tr>
				<tr>
					<td>订单号:orderNo</td>
					<td><input type="text" name="orderNo"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<br />
	<br />
	<p>【浙江】第一步，获取验证码</p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findZheJiangValidatedCode.do">
			<table>
				<tr>
					<td>国税局名称:IRSName</td>
					<td><input type="text" name="IRSName"/></td>
				</tr>
				<tr>
					<td>发票代码:billCode</td>
					<td><input type="text" name="billCode"/></td>
				</tr>
				<tr>
					<td>发票号码:billNo</td>
					<td><input type="text" name="billNo"/></td>
				</tr>
				<tr>
					<td>开票方识别号:taxpayerId</td>
					<td><input type="text" name="taxpayerId"/></td>
				</tr>
				<tr>
					<td>开票日期:billingTime</td>
					<td><input type="text" name="billingTime"/></td>
				</tr>
				<tr>
					<td>价税合计金额:billingAmount</td>
					<td><input type="text" name="billingAmount"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<br />
	<br />
	<p>【浙江】第二步，获取结果</p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findZheJiangBillInfo.do">
			<table>
				<tr>
					<td>验证码:validatedCode</td>
					<td><input type="text" name="validatedCode"/></td>
				</tr>
				<tr>
					<td>订单号:orderNo</td>
					<td><input type="text" name="orderNo"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<p>【山东（除青岛外）2013年10月后】第一步，获取 验证码 </p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findShanDongAfter201310ValidatedCode.do">
			<table>
				<tr>
					<td>国税局名称:IRSName</td>
					<td><input type="text" name="IRSName"/></td>
				</tr>
				<tr>
					<td>发票代码:billCode</td>
					<td><input type="text" name="billCode"/></td>
				</tr>
				<tr>
					<td>发票号码:billNo</td>
					<td><input type="text" name="billNo"/></td>
				</tr>
				<tr>
					<td>开票方识别号:taxpayerId</td>
					<td><input type="text" name="taxpayerId"/></td>
				</tr>
				<tr>
					<td>纳税人名称:taxpayerName</td>
					<td><input type="text" name="taxpayerName"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<br />
	<br />
	<p>【山东（除青岛外）2013年10月后】第二步，获取结果</p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findShanDongAfter201310BillInfo.do">
			<table>
				<tr>
					<td>验证码:validatedCode</td>
					<td><input type="text" name="validatedCode"/></td>
				</tr>
				<tr>
					<td>订单号:orderNo</td>
					<td><input type="text" name="orderNo"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<br />
	<br />
	<p>【山东（除青岛外）2013年10月前】 第一步，获取验证码 </p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findShanDongBefore201310ValidatedCode.do">
			<table>
				<tr>
					<td>国税局名称:IRSName</td>
					<td><input type="text" name="IRSName"/></td>
				</tr>
				<tr>
					<td>发票代码:billCode</td>
					<td><input type="text" name="billCode"/></td>
				</tr>
				<tr>
					<td>发票号码:billNo</td>
					<td><input type="text" name="billNo"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<br />
	<br />
	<p>【山东（除青岛外）2013年10月前】第二步，获取结果</p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findShanDongBefore201310BillInfo.do">
			<table>
				<tr>
					<td>验证码:validatedCode</td>
					<td><input type="text" name="validatedCode"/></td>
				</tr>
				<tr>
					<td>订单号:orderNo</td>
					<td><input type="text" name="orderNo"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<br />
	<br />
	<p>【青岛】第一步，获取验证码</p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findQingdaoValidatedCode.do">
			<table>
				<tr>
					<td>国税局名称:IRSName</td>
					<td><input type="text" name="IRSName"/></td>
				</tr>
				<tr>
					<td>发票代码:billCode</td>
					<td><input type="text" name="billCode"/></td>
				</tr>
				<tr>
					<td>发票号码:billNo</td>
					<td><input type="text" name="billNo"/></td>
				</tr>
				<tr>
					<td>开票方识别号:taxpayerId</td>
					<td><input type="text" name="taxpayerId"/></td>
				</tr>
				<tr>
					<td>身份证号码/组织机构代码:id</td>
					<td><input type="text" name="id"/></td>
				</tr>
				<tr>
					<td>价税合计金额:billingAmount</td>
					<td><input type="text" name="billingAmount"/></td>
				</tr>
				<tr>
					<td>增值税税额:addedTax</td>
					<td><input type="text" name="addedTax"/></td>
				</tr>
				<tr>
					<td>开票日期:billingTime</td>
					<td><input type="text" name="billingTime"/></td>
				</tr>
				<tr>
					<td>票面是否有税控码:haveTaxCode</td>
					<td><input type="text" name="haveTaxCode"/></td>
				</tr>
				<tr>
					<td>是否为青岛市网络发票管理系统<br>开具的机动车发票:isNetworkInvoice</td>
					<td><input type="text" name="isNetworkInvoice"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<br />
	<br />
	<p>【青岛】第二步，获取结果</p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findQingdaoBillInfo.do">
			<table>
				<tr>
					<td>验证码:validatedCode</td>
					<td><input type="text" name="validatedCode"/></td>
				</tr>
				<tr>
					<td>订单号:orderNo</td>
					<td><input type="text" name="orderNo"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<br />
	<br />
	<p>【北京】第一步，获取验证码</p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findBeiJingValidatedCode.do">
			<table>
				<tr>
					<td>国税局名称:IRSName</td>
					<td><input type="text" name="IRSName"/></td>
				</tr>
				<tr>
					<td>发票代码:billCode</td>
					<td><input type="text" name="billCode"/></td>
				</tr>
				<tr>
					<td>发票号码:billNo</td>
					<td><input type="text" name="billNo"/></td>
				</tr>
				<tr>
					<td>开票日期:billingTime</td>
					<td><input type="text" name="billingTime"/></td>
				</tr>
				<tr>
					<td>价税合计金额:billingAmount</td>
					<td><input type="text" name="billingAmount"/></td>
				</tr>
				<tr>
					<td>开票方识别号:taxpayerId</td>
					<td><input type="text" name="taxpayerId"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	<br />
	<br />
	<p>【北京】第二步，获取结果</p>
	<form method="post"
		action="<%=request.getContextPath()%>/bill/findBeiJingBillInfo.do">
			<table>
				<tr>
					<td>验证码:validatedCode</td>
					<td><input type="text" name="validatedCode"/></td>
				</tr>
				<tr>
					<td>订单号:orderNo</td>
					<td><input type="text" name="orderNo"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
						<font id="showResult"></font>
					</td>
				</tr> 
			</table>
	</form>
	
	
<p>行内获取【不区分省市的查询发票接口】第一步获取验证码</p>
<form method="post" action="<%=request.getContextPath()%>/bill/findValidatedCode.do">
	<table>
		<tr>
			<td>发票代码 billCode:</td>
			<td><input type="text" name="billCode"/></td>
		</tr>
		<tr>
			<td>发票号码billNo:</td>
			<td><input type="text" name="billNo"/></td>
		</tr>
        <tr>
            <td>开票日期 occurDate:</td>
            <td><input type="text" name="occurDate"/></td>
        </tr>
        <tr>
            <td>开票金额（不含税） amount:</td>
            <td><input type="text" name="amount"/></td>
        </tr>
		<tr>
			<td colspan="2">
				<!-- <input type="submit" value="Submit"/> -->
				<input type="button" onclick='toFindValidatedCode($(this.form).attr("action"),"findResult-bill")' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr>
	</table>
</form>


<p>行内获取【不区分省市的查询发票接口】第二步获取发票结果</p>
<form id="findResult-bill" method="post" action="<%=request.getContextPath()%>/bill/findResult.do">
	<table>
		<tr>
			<td>订单号 orderNo:</td>
			<td><input type="text" name="orderNo"/></td>
		</tr>
		<tr>
			<td>验证码 validatedCode:</td>
			<td><input type="text" name="validatedCode"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<!-- <input type="submit" value="Submit"/> -->
				<input type="button" onclick='toFindResult($(this.form).attr("action"))' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr> 
	</table>
</form>



</body>
<script src="<%=request.getContextPath()%>/res/jsputil.js"></script>
<script src="<%=request.getContextPath()%>/res/jsputil_bill.js"></script>
</html>