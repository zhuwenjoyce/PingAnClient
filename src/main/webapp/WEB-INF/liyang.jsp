<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- java class: LiYangClientController.java url: /liyang/test-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>测试|力洋</title>
<style type="text/css">
input[type='text']{ width:350px; height:15px; }
</style>
<script src="<%=request.getContextPath()%>/res/jquery-3.1.1.js"></script>
<script src="<%=request.getContextPath()%>/res/jquery.form.values.js"></script>
<script src="<%=request.getContextPath()%>/res/date.format.js"></script>
<script src="<%=request.getContextPath()%>/res/jsputil.js"></script>
</head>
<body>
<h3><a href="<%=request.getContextPath()%>">返回首页</a></h3>
<form method="post" action="<%=request.getContextPath()%>/liyang/findCXInfoByVIN.do">
	<table>
		<tr>
			<td>力洋车架查询</td>
		</tr>
		<tr>
			<td><label>VIN:</label><input name="vin" type="text"/></td>
		</tr>
		<tr>
			<td>
			<!-- <input type="submit" value="Submit"/><br/><br/> -->
			<input type="button"
			onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
			<font id="showResult"></font>
			</td>
		</tr>
	</table>
</form>

</body>
</html>