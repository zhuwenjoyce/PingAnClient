<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- java class: ClientController.java url: /controller/test  -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>测试|组织机构代码信息</title>
<style type="text/css">
input[type='text']{ width:350px; height:15px; }
</style>
<script src="<%=request.getContextPath()%>/res/jquery-3.1.1.js"></script>
<script src="<%=request.getContextPath()%>/res/jquery.form.values.js"></script>
<script src="<%=request.getContextPath()%>/res/date.format.js"></script>
<script src="<%=request.getContextPath()%>/res/jsputil.js"></script>
</head>
<body>
<h3><a href="<%=request.getContextPath()%>">返回首页</a></h3>
<p>findOrgMessage 行内获取【组织机构代码信息】，不需要验证码</p>
<form method="post" action="<%=request.getContextPath()%>/controller/findOrgMessage.do">
	<table>
		<tr>
			<td>enterpriseName:</td>
			<td><input type="text" name="enterpriseName"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<!-- <input type="submit" value="Submit"/> -->
				<input type="button" onclick='toFormSubmit($(this.form).attr("action"))' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr>
	</table>
</form>


</body>
</html>