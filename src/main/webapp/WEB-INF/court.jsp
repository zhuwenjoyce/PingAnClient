<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- java class: ClientController.java url: /controller/test  -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>测试|法院执行信息（个人、企业）</title>
<style type="text/css">
input[type='text']{ width:350px; height:15px; }
</style>
<script src="<%=request.getContextPath()%>/res/jquery-3.1.1.js"></script>
<script src="<%=request.getContextPath()%>/res/jquery.form.values.js"></script>
<script src="<%=request.getContextPath()%>/res/date.format.js"></script>
</head>
<body>
<h3><a href="<%=request.getContextPath()%>">返回首页</a></h3>
<p>行内获取【法院执行信息（个人、企业）】第一步，第一次获得验证码</p>
<form id="step1" method="post" action="<%=request.getContextPath()%>/controller/findExecutorValidated.do">
	<table>
		<tr>
			<td>被执行人姓名/名称Name:</td>
			<td><input type="text" name="name"/></td>
		</tr>
		<tr>
			<td>身份证号码/组织机构代码Code:</td>
			<td><input type="text" name="code"/></td>
		</tr>
		<tr>
			<td>查询途径route</td>
			<td><select name="route"><option value="官方">官方</option><option value="百度">百度</option></select></td>
		</tr>
		<tr>
			<td colspan="2">
				<!-- <input type="submit" value="Submit"/> -->
				<input type="button" onclick='toFormSubmit1($(this.form).attr("action"))' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr>
	</table>
</form>

<p>行内获取【法院执行信息（个人、企业）】第二步，根据验证码再次获得验证码</p>
<form id="step2" method="post" action="<%=request.getContextPath()%>/controller/findExecutorValidated2.do">
	<table>
		<tr>
			<td>被执行人姓名/名称Name:</td>
			<td><input type="text" name="name"/></td>
		</tr>
		<tr>
			<td>身份证号码/组织机构代码Code:</td>
			<td><input type="text" name="code"/></td>
		</tr>
		<tr>
			<td>验证码validatedCode</td>
			<td><input type="text" name="validatedCode"/></td>
		</tr>
		<tr>
			<td>orderNo:</td>
			<td><input type="text" name="orderNo"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<!-- <input type="submit" value="Submit"/> -->
				<input type="button" onclick='toFormSubmit2($(this.form).attr("action"))' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr>
	</table>
</form>

<p>行内获取【法院执行信息（个人、企业）】第三步，输入验证码，返回结果</p>
<form id="step3" method="post" action="<%=request.getContextPath()%>/controller/findExecutorMessage.do">
	<table>
		<tr>
			<td>被执行人姓名/名称Name:</td>
			<td><input type="text" name="name"/></td>
		</tr>
		<tr>
			<td>身份证号码/组织机构代码Code:</td>
			<td><input type="text" name="code"/></td>
		</tr>
		<tr>
			<td>validatedCode</td>
			<td><input type="text" name="validatedCode"/></td>
		</tr>
		<tr>
			<td>orderNo:</td>
			<td><input type="text" name="orderNo"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<!-- <input type="submit" value="Submit"/> -->
				<input type="button" onclick='toFormSubmit3($(this.form).attr("action"))' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr>
	</table>
</form>
</body>
<script src="<%=request.getContextPath()%>/res/jsputil_court.js"></script>
</html>