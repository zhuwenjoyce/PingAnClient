<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- java class: PolicyClientController.java url: /policy/test-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>测试|保单 </title>
<style type="text/css">
input[type='text']{ width:350px; height:15px; }
</style>
<script src="<%=request.getContextPath()%>/res/jquery-3.1.1.js"></script>
<script src="<%=request.getContextPath()%>/res/jquery.form.values.js"></script>
<script src="<%=request.getContextPath()%>/res/date.format.js"></script>
</head>
<body>
<h3><a href="<%=request.getContextPath()%>">返回首页</a></h3>
<p><b>太平洋保险第一步，获取验证码</b></p>
<form method="POST" action="<%=request.getContextPath()%>/policy/findCPICValidatedCode.do">
	<table>
		<tr>
			<td>保险公司名字：companyName</td>
			<td><input type="text" name="companyName"/></td>
		</tr>
		<tr>
			<td>证件类型：certificateType</td>
			<td><input type="text" name="certificateType"/></td>
		</tr>
		<tr>
			<td>证件号：id</td>
			<td><input type="text" name="id"/></td>
		</tr>
		<tr>
			<td>保单号：policyNo</td>
			<td><input type="text" name="policyNo"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" onclick='toFindValidatedCode($(this.form).attr("action"),"findResult-CPIC")' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr> 
	</table>
</form>
<p><b>太平洋保险第二步，获取结果</b></p>
<form id="findResult-CPIC" method="POST" action="<%=request.getContextPath()%>/policy/findCPICMessage.do">
	<table>
		<tr>
			<td>验证码 validatedCode:</td>
			<td><input type="text" name="validatedCode"/></td>
		</tr>
		<tr>
			<td>orderNo:</td>
			<td><input type="text" name="orderNo"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" onclick='toFindResult($(this.form).attr("action"))' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr> 
	</table>
</form>
<p><b>平安财险第一步，获取验证码</b></p>
<form method="POST" action="<%=request.getContextPath()%>/policy/findPingAnPropertyCasualtyValidatedCode.do">
	<table>
		<tr>
			<td>保险公司名字companyName：</td>
			<td><input type="text" name="companyName"/></td>
		</tr>
		<tr>
			<td>保单类型policyType：</td>
			<td><input type="text" name="policyType"/></td>
		</tr>
		<tr>
			<td>保单号policyNo：</td>
			<td><input type="text" name="policyNo"/></td>
		</tr>
		<tr>
			<td>保单验真码PolicyVerificationCode：</td>
			<td><input type="text" name="PolicyVerificationCode"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" onclick='toFindValidatedCode($(this.form).attr("action"),"findResult-PingAnPropertyCasualty")' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr> 
	</table>
</form>
<p><b>平安财险第二步，获取结果</b></p>
<form id="findResult-PingAnPropertyCasualty" method="POST" action="<%=request.getContextPath()%>/policy/findPingAnPropertyCasualtyMessage.do">
	<table>
		<tr>
			<td>验证码 validatedCode:</td>
			<td><input type="text" name="validatedCode"/></td>
		</tr>
		<tr>
			<td>orderNo:</td>
			<td><input type="text" name="orderNo"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" onclick='toFindResult($(this.form).attr("action"))' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr> 
	</table>
</form>
<p><b>平安人寿第一步，获取验证码</b></p>
<form method="POST" action="<%=request.getContextPath()%>/policy/findValidatedCode/lifeInsurance.do">
	<table>
		<tr>
			<td>保险公司名字：companyName</td>
			<td><input type="text" name="companyName"/></td>
		</tr>
		<tr>
			<td>投保人名称userName：</td>
			<td><input type="text" name="userName"/></td>
		</tr>
		<tr>
			<td>投保人生日birthday ：</td>
			<td><input type="text" name="birthday"/></td>
		</tr>
		<tr>
			<td>保单号policyNo：</td>
			<td><input type="text" name="policyNo"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" onclick='toFindValidatedCode($(this.form).attr("action"),"findResult-lifeInsurance")' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr> 
	</table>
</form>
<p><b>平安人寿第二步，获取结果</b></p>
<form id="findResult-lifeInsurance" method="POST" action="<%=request.getContextPath()%>/policy/findResult/lifeInsurance.do">
	<table>
		<tr>
			<td>验证码 validatedCode:</td>
			<td><input type="text" name="validatedCode"/></td>
		</tr>
		<tr>
			<td>orderNo:</td>
			<td><input type="text" name="orderNo"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" onclick='toFindResult($(this.form).attr("action"))' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr> 
	</table>
</form>
</body>
<script src="<%=request.getContextPath()%>/res/jsputil_policy.js"></script>
</html>