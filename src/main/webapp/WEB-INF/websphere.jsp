<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- java class: ClientController.java url: /controller/test  -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>测试|企业工商信息</title>
<style type="text/css">
input[type='text']{ width:350px; height:15px; }
</style>
<script src="<%=request.getContextPath()%>/res/jquery-3.1.1.js"></script>
<script src="<%=request.getContextPath()%>/res/jquery.form.values.js"></script>
<script src="<%=request.getContextPath()%>/res/date.format.js"></script>
</head>
<body>
<h3><a href="<%=request.getContextPath()%>">返回首页</a></h3>
<p>websphere服务器测试</p>
<form method="post" action="<%=request.getContextPath()%>/ws/start/post.do">
	<table>
		<tr>
			<td>城市city:</td>
			<td><input type="text" name="city" value="上海市"/></td>
		</tr>
		<tr>
			<td>姓名name:</td>
			<td><input type="text" name="name" value="孙俪"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" onclick='toSubmitForm($(this.form).attr("action"))' value="ajax提交" />
				<font id="showResult"></font>
			</td>
		</tr>
	</table>
</form>

</body>
<script src="<%=request.getContextPath()%>/res/jsputil_websphere.js"></script>
</html>