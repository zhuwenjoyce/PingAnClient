var toFormSubmit = function(action) {
		var formElement = $("form[action=\""+action+"\"]");
		var params = formElement.values();
		
		var resultDiv = formElement.find("#showResult");
		
		//显示提示信息
		resultDiv.html(new Date().Format("HH:mm:ss.S") + "请求参数为："+JSON.stringify(params)+"，等待结果...");
		
		$.ajax({
			url : action,
			method : "POST",
			dataType : "json",
			data: params,
			success : function(data) {
				//打印返回内容
				console.log("result data==", data);
				
				//返回验证码
				if(data.status == 200 && data.data.validatedCode){
					//输出验证码图片
					var img = "<img src=\"data:image/png;base64,"+data.data.validatedCode+"\" >";
					
					//把太长的验证码去掉
					data.data.validatedCode = "";
					var resultContent = "返回时间："+new Date().Format("HH:mm:ss.S")+"<br/>返回数据："+JSON.stringify(data) +"<br/>请求参数为："+JSON.stringify(params);
					
					resultDiv.html("验证码："+img+resultContent);
					
					//填充第二个步骤的input框
					$("#resultForm").find("input[name='city']").val(params.city);
					$("#resultForm").find("input[name='orderNo']").val(data.orderNo);
					$("#resultForm").find("input[name='enterpriseName']").val(params.enterpriseName);
					
				}else{
					//返回错误信息
					var resultContent = "返回时间："+new Date().Format("HH:mm:ss.S")+"<br/>返回数据："+JSON.stringify(data) +"<br/>请求参数为："+JSON.stringify(params);
					resultDiv.html(resultContent);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}
		}).done(function(result) {
			//console.log("result==", result);
		});
	}
