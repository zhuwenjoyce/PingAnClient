var lastParam = {};
var toFormSubmit = function(action) {
		var formElement = $("form[action=\""+action+"\"]");
		var params = formElement.values();
		
		var resultDiv = formElement.find("#showResult");
		
		//校验表单是否重复提交
		if(isRepeatSubmit(params)){
			resultDiv.html("表单重复提交！参数为："+JSON.stringify(params));
			return; 
		}else{
			console.log(new Date().Format("yyyy-MM-dd HH:mm:ss.S")+" 表单提交！参数为："+JSON.stringify(params));
		}
		lastParam = params;
		
		//显示提示信息
		resultDiv.html(new Date().Format("HH:mm:ss.S") + "请求参数为："+JSON.stringify(params)+"，等待结果...");
		
		$.ajax({
			url : action,
			method : "POST",
			dataType : "json",
			data: params,
			success : function(data) {
				//打印返回内容
//				console.log("result data==", data);
				
				//返回验证码
				if(data.status == 200 && data.data.validatedCode){
					//输出验证码图片
					var img = "<img src=\"data:image/png;base64,"+data.data.validatedCode+"\" >";
					
					//把太长的验证码去掉
					data.data.validatedCode = "";
					var resultContent = "返回时间："+new Date().Format("HH:mm:ss.S")+"<br/>返回数据："+JSON.stringify(data) +"<br/>请求参数为："+JSON.stringify(params);
					
					resultDiv.html("验证码："+img+resultContent);
				}else{
					//返回错误信息
					var resultContent = "返回时间："+new Date().Format("HH:mm:ss.S")+"<br/>返回数据："+JSON.stringify(data) +"<br/>请求参数为："+JSON.stringify(params);
					resultDiv.html(resultContent);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}
		}).done(function(result) {
			//console.log("result==", result);
		});
	}

var isRepeatSubmit = function(params){
	for(var key in params ){  
	    if(params[key] != lastParam[key]){
//	    	console.log("return false. params["+key+"]= "+params[key]+", lastParam["+key+"]="+lastParam[key]);
	    	return false;
	    }
	}  
//	console.log("return true.");
	return true;
}
