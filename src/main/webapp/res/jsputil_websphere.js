var toSubmitForm = function(action) {
	var formElement = $("form[action=\"" + action + "\"]");
	var params = formElement.values();
	var resultDiv = formElement.find("#showResult");
	// 显示提示信息
	resultDiv.html(new Date().Format("HH:mm:ss.S") + "请求参数为："
			+ JSON.stringify(params) + "，等待结果...");
	$.ajax(
			{
				url : action,
				method : "POST",
				dataType : "json",
				data : params,
				success : function(data) {
					// 打印返回内容
					console.log("Ajax END. return data===", data);
					var resultContent = "返回时间："
							+ new Date().Format("HH:mm:ss.S") + "<br/>返回数据："
							+ JSON.stringify(data) + "<br/>请求参数为："
							+ JSON.stringify(params);
					resultDiv.html(resultContent);
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert(errorThrown);
				}
			}).done(function(result) {
		// console.log("result==", result);
	});
}
