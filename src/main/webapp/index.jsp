<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- java class: ClientController.java url: /controller/test  -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Client</title>
<style type="text/css">
input[type='text']{ width:350px; height:15px; }
div{padding:5px;}
</style>
<script src="<%=request.getContextPath()%>/res/jquery-3.1.1.js"></script>
<script src="<%=request.getContextPath()%>/res/jquery.form.values.js"></script>
<script src="<%=request.getContextPath()%>/res/date.format.js"></script>
</head>
<body>

<h2>PingAnClient is running!</h2>

<ol>

<div><a href="<%=request.getContextPath()%>/bill/test.do"><li>
测试 | 发票、国税</li></a> </div>

<div><a href="<%=request.getContextPath()%>/court/test.do"><li>
测试 | 法院执行信息（个人、企业）</li></a> </div>

<div><a href="<%=request.getContextPath()%>/enterprise/test.do"><li>
测试 | 工商企业</li></a> </div>

<div><a href="<%=request.getContextPath()%>/liyang/test.do"><li>
测试 | 力洋车架查询</li></a> </div>

<div><a href="<%=request.getContextPath()%>/organization/test.do"><li>
测试 | 组织机构代码信息</li></a> </div>

<div><a href="<%=request.getContextPath()%>/policy/test.do"><li>
测试 | 保单（太平洋保险，平安财险，平安人寿）</li></a> </div>

<div><a href="<%=request.getContextPath()%>/vehicle/test.do"><li>
测试 | 违章、计分、报废、车价、汽车图片</li></a> </div>

<div><a href="<%=request.getContextPath()%>/phone/test.do"><li>
测试 | 电话号码</li></a> </div>

<div><a href="<%=request.getContextPath()%>/ws/test.do"><li>
测试 | websphere服务器测试</li></a> </div>

</ol>

</body>

</html>